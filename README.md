# SSI Overview

![Digicampus](https://www.dedigicampus.nl/img/logo-digicampus.svg)

![Dutch Blockchain Coalition](https://dutchblockchaincoalition.org/assets/images/Logo-DBC.png)

This repo provides a short overview of SSI-related technologies, frameworks along with links to information.
In addition, points of contact that are present for Odyssey 2020 are listed.

These lists are work in progress, feel free to submit a Pull Request with additional technologies or information. 

## Platforms

|Platform|Description|Documentation|Online channel|Point of contact|
|---|---|---|---|---|
|[Holochain](http://holochain.com/)|Distributed Hash Table|[Storybook](https://holochain.github.io/holochain-ui/?selectedKind=HoloVault&selectedStory=Keep%20your%20data%20private&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Fnotes%2Fpanel)|[Forum](https://forum.holochain.org/)| Eric Bear |
|[HyperLedger Indy](https://www.hyperledger.org/projects/hyperledger-indy)|Distributed Ledger|[Walkthrough](https://github.com/hyperledger/indy-sdk/blob/master/docs/getting-started/indy-walkthrough.md)|[Rocket Chat](https://chat.hyperledger.org/channel/indy)|   |

## Frameworks

|Framework|Description|Documentation|Online channel|Point of contact|
|---|---|---|---|---|
|[Discipl](https://discipl.org/)|Build solutions on top of SSI|[Main repo](https://github.com/discipl/main)|[Slack (see main repo for invite link)](https://discipl.slack.com)|[Bas Kaptijn](https://github.com/bkaptijn) or [Pim Otte](https://github.com/pimotte)|
|[Universal Ledger Agent](https://github.com/WebOfTrustInfo/rwot8-barcelona/blob/master/topics-and-advance-readings/universal-ledger-agent.md)|Interoperability between SSI solutions|[Main repo](https://github.com/rabobank-blockchain), also API available|[Slack invite link](https://join.slack.com/t/rabobankssi/shared_invite/zt-d58zvo4j-MjXbGJ1UrcOhcGHREQAbsA)|[Marnix van den Bent (technical)](https://www.linkedin.com/in/marnix-van-den-bent/) or [David Lamers (use cases)](https://www.linkedin.com/in/lamersdavid/)|

## Technology and tools

|Tech|Description|Documentation|Online channel|Point of contact|
|---|---|---|---|---|
|[Tangem made-for-blockchain NFC](https://www.gimly.io/blog/odyssey-tangem)|Provide physical objects with digital IDs with Tangem's made-for-blockchain NFC chips.|[Gimly Github](https://github.com/Gimly-Blockchain/Tangem-Blockchain-NFC)|[Discord](https://discord.gg/RXK992X)| [Caspar Roelofs (Gimly)](mailto:tangem@gimly.io?SUBJECT=Tangem%20for%20Odyssey%20SSI-toolbox) |

## [Standards](./standards.md)
- [List of standards](./standards.md) provided by NEN

## Other resources
- [Data sets about companies from Chamber of Commerce](https://www.kvk.nl/advies-en-informatie/innovatie/kvk-innovatielab/) - KvK website (Dutch)
- [Overview of Decentralized Identity Standards](https://medium.com/decentralized-identity/overview-of-decentralized-identity-standards-f82efd9ab6c7) - Medium article
