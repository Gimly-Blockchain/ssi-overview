# Odyssey Hackathon 2020 – Standards

## What is a standard?

Standards are agreements between multiple parties. Standards are developed by groups of experts. These experts work together in technical committees (TCs) and working groups (WGs), which are organized by subject. Each technical committee manages multiple standards. Please find more information about standardization on [nen.nl](https://www.nen.nl/Over-NEN.htm) and [iso.org](https://www.iso.org/standards.html).

Standards are developed at 3 levels:

1. In The Netherlands at NEN
2. In Europe by CEN and CENELEC
3. Globally by ISO and IEC

In this overview, information on standardization is organized by subject. Most standards are available in the English language. However, local information on Dutch standardization is only available in Dutch.

## How can my Odyssey team access standards?

- Odyssey teams can access standards through [NEN Connect](https://connect.nen.nl/Home/Detail).
- Odyssey participants can sign up to NEN Connect via [dyssey@nen.nl](mailto:odyssey@nen.nl).
- Help on NEN Connect:
  - [Video](https://vimeo.com/389741758) on how to sign up and download standards;
  - [User guides](https://connect.nen.nl/portal/Kennis/Gebruiker/en);
  - How to [search within NEN Connect](https://connect.nen.nl/Help/Search).

## Which standards are relevant to my Odyssey challenge?

For each subject an immense number of standards exist. Therefore we provide you with starting points for your search for relevant standards.

### Tips to find relevant standards

- **BROWSE** standards per subject through ISO/CEN technical committee pages and NEN committee pages via links below.
- **SEARCH** standards by ID (e.g. &#39;ISO/IEC 20546&#39;) or by key word (e.g. &#39;big data&#39;) on [NEN Connect](https://connect.nen.nl/Home/Detail).
- **FIND** information about [ICT standardization in the Netherlands](https://www.nen.nl/Normontwikkeling/Doe-mee/Normcommissies-en-nieuwe-trajecten/Normcommissies-ICT.htm) (Dutch). This portal links to information on standardization, organized by subject.

### Data

#### Artificial Intelligence and Big data

- **Key words** – Artificial Intelligence (AI), Machine Learning, Risk Management, Bias, neural networks, Big data analytics, Governance implications of the use of artificial intelligence by organizations, Use cases.
- [NEN committee Artificial Intelligence](https://www.nen.nl/Normontwikkeling/Doe-mee/Normcommissies-en-nieuwe-trajecten/AI.htm) (Dutch)
- [Standards by ISO/IEC JTC 1/SC 42 Artificial Intelligence](https://www.iso.org/committee/6794475/x/catalogue/p/1/u/1/w/0/d/0)

#### Blockchain

- **Key words** – Security techniques, Interoperability, Security, privacy and identity, personally identifiable information protection, Smart contracts, Governance, Use cases, identity management using blockchain and distributed ledger technologies.
- [NEN committee Blockchain](https://www.nen.nl/Normontwikkeling/Doe-mee/Normcommissies-en-nieuwe-trajecten/Normcommissies-ICT/Blocktober.htm) (Dutch)
- [Standards by ISO/TC 307 Blockchain and DLT](https://www.iso.org/committee/6266604/x/catalogue/p/1/u/1/w/0/d/0)

#### Cloud Computing and Distributed Platforms

- **Key words** – Cloud computing, edge computing.
- [NEN committee Cloud computing](https://www.nen.nl/Normontwikkeling/Cloudcomputing.htm) (Dutch)
- [ISO standards by ISO/IEC JTC 1/SC 38 Cloud Computing and Distributed Platforms](https://www.iso.org/committee/601355/x/catalogue/p/1/u/1/w/0/d/0)
- [ISO standard Edge computing landscape](https://www.iso.org/standard/74846.html)

#### Automation systems and integration

- **Key words** – Interoperability, integration, and architectures for enterprise systems and automation applications, Digital Twin, Information and data quality: Concepts and measuring.
- [ISO standards by ISO/TC 184 Automation systems and integration](https://www.iso.org/committee/54110.html)

#### Archives and records management

- **Key words** – Data exchange, structured data environments, Metadata, Management systems for records, Records in the cloud.
- [NEN committee Archives and records management](https://www.nen.nl/Normontwikkeling/Informatiearchiefmanagement.htm) (Dutch)
- [Standards by ISO ISO/TC 46/SC 11 Archives/records management](https://www.iso.org/committee/48856/x/catalogue/p/1/u/1/w/0/d/0)

#### Building Information Modelling

- **Key words** – Standardization in the field of structured semantic life-cycle information for the built environment (also relevant for data exchange within &#39;smart cities&#39;), Building Information Modelling (BIM).
- [Standards by CEN/TC 442 - Building Information Modelling (BIM)](https://standards.cen.eu/dyn/www/f?p=204:32:0::::FSP_ORG_ID,FSP_LANG_ID:1991542,25&amp;cs=1085D2CA41E34A1C2DA860E5234AA5A97)

#### Information security, cybersecurity and privacy protection

- **Key words** – Security requirements, Management of information and ICT security, Cryptographic and other security mechanisms, Security aspects of identity management, biometrics and privacy, authenticated encryption, digital signature techniques, protection of PII, Biometric information protection.
- [Standards by ISO/IEC JTC 1/SC 27 Information security, cybersecurity and privacy protection](https://www.iso.org/committee/45306/x/catalogue/p/1/u/1/w/0/d/0)

### Identity and identification

#### Automatic identification

- **Key words** – Automatic identification and data capture techniques, RFID, bar codes, EAN, Unique identification, Product tagging, Data structure — Unique identification for the Internet of Things.
- [NEN committee Automatic identification](https://www.nen.nl/Normontwikkeling/ICT/Automatische-Identificatie.htm) (Dutch)
- [Standards by ISO/IEC JTC 1/SC 31 Automatic identification and data capture techniques](https://www.iso.org/committee/45332/x/catalogue/p/1/u/1/w/0/d/0)
- [Standards by CEN/TC 225 - AIDC technologies](https://standards.cen.eu/dyn/www/f?p=204:32:0::::FSP_ORG_ID,FSP_LANG_ID:6206,25&amp;cs=136D1799132ED1E13E56D38C2E645A7D2)

#### Biometrics

- **Key words** – Biometric application programming interfaces, biometric data interchange formats, Biometric sensor function provider interface.
- [NEN committee Biometrics](https://www.nen.nl/Normontwikkeling/Doe-mee/Normcommissies-en-nieuwe-trajecten/Biometrie.htm) (Dutch)
- [Standards by ISO/IEC JTC 1/SC 37 Biometrics](https://www.iso.org/committee/313770/x/catalogue/p/1/u/1/w/0/d/0)

#### Personal identification and IDs

- **Key words** – personal identification, identity cards, security devices, tokens, interfaces and international interchange, mobile driver&#39;s license
- [NEN committee Personal identification and IDs](https://www.nen.nl/Normontwikkeling/Doe-mee/Normcommissies-en-nieuwe-trajecten/Persoonlijke-identificatie-en-identificatiekaarten.htm)
- [Standards by ISO/IEC JTC 1/SC 17 Cards and security devices for personal identification](https://www.iso.org/committee/45144/x/catalogue/p/1/u/1/w/0/d/0)

#### Self sovereign identity (SSI)

- The [European Self Sovereign Identity framework (ESSIF)](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/2019/12/16/EBSI+-+ESSIF+-+Stakeholder+meeting) is a European Blockchain Partnership between The Netherlands, Germany and Belgium. This project aims to develop &quot;self-sovereign identity for citizens and organizations in Europe and the world&quot;.
- The [Sovrin foundation](https://www.linkedin.com/company/sovrin-foundation/) are working on &#39;self-sovereign identity&#39;.
- The [Decentralised Identity Foundation](https://identity.foundation/working-groups/identifiers-names-discovery.html) (DIF) is working on development of protocols and systems that enable creation, resolution, and discovery of decentralized identifiers and names across underlying decentralized systems, like blockchains and distributed ledgers.
- W3C is working on standardization of [Decentralized Identifiers (DIDs)](https://www.w3.org/TR/did-core/). This can facilitate identification within Blockchains. DIDs are an important building block for digital identities.
- TNO is working on [Self-Sovereign Identity](https://blockchain.tno.nl/blog/self-sovereign-identity-the-good-the-bad-and-the-ugly/).

### Sensors

#### Internet of Things sensor networks

- **Key words** – Sensor network testing framework, Sensor networks — Services and interfaces supporting collaborative information processing in intelligent sensor networks, Interoperability for internet of things systems, Internet of things (IoT) use cases.
- [NEN committee on Internet of Things](https://www.nen.nl/Normontwikkeling/Doe-mee/Normcommissies-en-nieuwe-trajecten/Internet-of-Things.htm) (Dutch)
- [Standards by ISO/IEC JTC 1/SC 41 Internet of Things and related technologies](https://www.iso.org/committee/6483279/x/catalogue/p/1/u/1/w/0/d/0)

#### Sensor security

- [ISO/IEC 29180:2012 [ISO/IEC 29180:2012] Information technology — Telecommunications and information exchange between systems — Security framework for ubiquitous sensor networks](https://www.iso.org/standard/45259.html)

### Smart cities

- **Key words** – Smart community infrastructures, mobility.
- **ISO and smart cities** - Building a smart city is highly complex. [Learn about how ISO standards help to make cities smarter](https://www.iso.org/publication/PUB100423.html).
- [NEN committee on Smart cities, NEN](https://www.nen.nl/Normontwikkeling/Doe-mee/Normcommissies-en-nieuwe-trajecten/NEN-smart-cities.htm) (Dutch)
- [Standards by ISO/TC 268 Sustainable cities and communities](https://www.iso.org/committee/656906.html)
- [Standards by CEN/TC 278/WG 17 - Mobility integration](https://standards.cen.eu/dyn/www/f?p=204:32:0::::FSP_ORG_ID,FSP_LANG_ID:2047388,25&amp;cs=15A4FAD86BFE8FF116CD58CAD43637C42)
- [Standards by CEN/TC 278/WG 4 - Traffic and traveller information (TTI)](https://standards.cen.eu/dyn/www/f?p=204:32:0::::FSP_ORG_ID,FSP_LANG_ID:7919,25&amp;cs=12BE02A5AE461026C44186B2B5DB00215)
- Technical standardization in the Netherlands: [pen urban platforms](https://connect.nen.nl/Standard/Detail/3625204?compId=16013&amp;collectionId=0).
- **Sensor register** – As of yet, no central sensor register is available. Various parties are working on disclosure of and linking of sensor data in public spaces.